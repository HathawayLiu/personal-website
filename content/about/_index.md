+++
title = "About Me"
+++
<div style="display: flex; align-items: flex-start;">
    <!-- Profile Picture -->
    <div style="flex: 0 0 auto; margin-right: 40px;">
        <img src="/about/hathy.png" alt="Hathaway Liu" style="width: 250px;">
    </div>

<!-- Bio Information -->
<div style="flex: 1 1 auto;">
<h1>Hathaway(Xinyan) Liu</h1>

<h2>Contact Information</h2>
    <ul>
        <li>Email: <a href="mailto:hathaway.liu@duke.edu">hathaway.liu@duke.edu</a></li>
        <li>LinkedIn: <a href="https://www.linkedin.com/in/hathaway-liu/">Hathaway Liu</a></li>
    </ul>

<h2>Education</h2>
    <ul>
        <li>Graduate school: Duke University, Master in Statistical Science</li>
        <li>Undergraduate school: University of Washington, Seattle</li>
    </ul>

<h2>Experience</h2>
    <ul>
        <li>Former Marketing Intern in EasyTransfer</li>
    </ul>

<h2>Publications</h2>
        <ul>
            <li>Independent author, "Fast Recommender System Combining Global and Local Information", accepted by 2022 2nd International Conference on Big Data, Artificial Intelligence and Risk Management (ICBAR2022)</li>
        </ul>
    </div>
</div>
